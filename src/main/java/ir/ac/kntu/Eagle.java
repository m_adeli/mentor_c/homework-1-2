package ir.ac.kntu;

public class Eagle extends Bird {

    public Eagle(Integer age, Double beakLength) {
        super(age, beakLength);
    }

    @Override
    void eatFood() {
        System.out.println("I am eating Fish.");
    }


    @Override
    public void makeSound() {
        System.out.println("gheeeeeeee");
    }

    @Override
    public void takeOff() {
        if (!getFlight()) {
            setFlight(true);
            flyForward(20D);
            flyHigher(20D);
        }
    }

    @Override
    public void landing() {
        if (getFlight()) {
            flyForward(2 * getZ());
            flyLower(getZ());
            setFlight(false);
        }
    }
}
