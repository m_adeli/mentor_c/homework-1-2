package ir.ac.kntu;

public class Hen extends Bird {

    public Hen(Integer age, Double beakLength) {
        super(age, beakLength);
    }

    @Override
    public void flyHigher(Double amount) {
        canNotFly();
    }

    @Override
    public void flyLower(Double amount) {
        canNotFly();
    }

    @Override
    public void flyRight(Double amount) {
        canNotFly();
    }

    @Override
    public void flyForward(Double amount) {
        canNotFly();
    }

    @Override
    public void flyLeft(Double amount) {
        canNotFly();
    }

    private void canNotFly() {
        System.out.println("I can't fly.");
    }

    @Override
    public void takeOff() {
        moveForward(5D);
        moveUp(5D);
    }

    @Override
    public void landing() {
        moveForward(2 * getZ());
        moveDown(getZ());
    }

    @Override
    void eatFood() {
        System.out.println("I am eating seed.");
    }

    @Override
    public void makeSound() {
        System.out.println("qod qod qoda");
    }

    @Override
    public String toString() {
        return "I am " + getAge() + " years old.\n" +
                "My beak length is " + getBeakLength() + ".'\n" +
                "(x: " + getX() +
                ", y: " + getY() +
                ", z: " + getZ() + ")";
    }
}
