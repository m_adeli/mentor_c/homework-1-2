package ir.ac.kntu;

public interface Movable {
    void moveUp(Double amount);

    void moveDown(Double amount);

    void moveRight(Double amount);

    void moveLeft(Double amount);

    void moveForward(Double amount);

    void moveBackward(Double amount);
}
