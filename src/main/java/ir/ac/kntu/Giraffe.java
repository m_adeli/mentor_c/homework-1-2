package ir.ac.kntu;

public class Giraffe extends Animal {

    public Giraffe(Integer age) {
        super(age);
    }

    @Override
    void eatFood() {
        System.out.println("I am eating Acacia");
    }
}
