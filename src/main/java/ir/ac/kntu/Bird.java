package ir.ac.kntu;

public abstract class Bird extends Animal implements Flyable, Voiced {
    private Double beakLength;
    private Boolean isFlight;

    public Bird(Integer age, Double beakLength) {
        super(age);
        this.beakLength = beakLength;
        this.isFlight = false;
    }

    public abstract void takeOff();

    public abstract void landing();

    @Override
    public void flyHigher(Double amount) {
        if (!getFlight())
            this.setZ(this.getZ() + amount);
        else
            System.out.println("I am on the floor");
    }

    @Override
    public void flyLower(Double amount) {
        if (getFlight())
            this.setZ(this.getZ() - amount);
        else
            System.out.println("I am on the floor");
    }

    @Override
    public void flyRight(Double amount) {
        if (getFlight())
            this.setY(this.getZ() + amount);
        else
            System.out.println("I am on the floor");
    }

    @Override
    public void flyLeft(Double amount) {
        if (getFlight())
            setY(getY() - amount);
        else
            System.out.println("I am on the floor");
    }

    @Override
    public void flyForward(Double amount) {
        if (getFlight())
            setX(getX() + amount);
        else
            System.out.println("I am on the floor");
    }


    @Override
    public void moveUp(Double amount) {
        if (!getFlight())
            setZ(getZ() + amount);
        else
            System.out.println("I am flying");
    }

    @Override
    public void moveDown(Double amount) {
        if (!getFlight())
            setZ(getZ() - amount);
        else
            System.out.println("I am flying");
    }

    @Override
    public void moveRight(Double amount) {
        if (!getFlight())
            setY(getY() + amount);
        else
            System.out.println("I am flying");
    }

    @Override
    public void moveLeft(Double amount) {
        if (!getFlight())
            setY(getY() - amount);
        else
            System.out.println("I am flying");
    }

    @Override
    public void moveForward(Double amount) {
        if (!getFlight())
            setX(getX() + amount);
        else
            System.out.println("I am flying");
    }

    @Override
    public void moveBackward(Double amount) {
        if (!getFlight())
            setX(getX() - amount);
        else
            System.out.println("I am flying");
    }


    public Double getBeakLength() {
        return beakLength;
    }

    public void setBeakLength(Double beakLength) {
        this.beakLength = beakLength;
    }

    public Boolean getFlight() {
        return isFlight;
    }

    public void setFlight(Boolean flight) {
        isFlight = flight;
    }
}
