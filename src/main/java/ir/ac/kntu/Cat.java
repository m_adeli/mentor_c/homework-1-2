package ir.ac.kntu;

public class Cat extends Animal implements Voiced {

    public Cat(Integer age) {
        super(age);
    }

    @Override
    void eatFood() {
        System.out.println("I am eating Mouse");
    }

    @Override
    public void makeSound() {
        System.out.println("Mewwwwww");
    }

    @Override
    public String toString() {
        return "I am " + getAge() + " years old\n" +
                "(x: " + getX() +
                ", y: " + getY() +
                ", z: " + getZ() + ")";
    }
}
