package ir.ac.kntu;

public class Kiwi extends Bird {
    public Kiwi(Integer age, Double beakLength) {
        super(age, beakLength);
    }

    @Override
    void eatFood() {
        System.out.println("I am eating Worms.");
    }

    private void canNotFly() {
        System.out.println("I can't fly.");
    }

    @Override
    public void flyHigher(Double amount) {
        canNotFly();
    }

    @Override
    public void flyLower(Double amount) {
        canNotFly();
    }

    @Override
    public void flyRight(Double amount) {
        canNotFly();
    }

    @Override
    public void flyLeft(Double amount) {
        canNotFly();
    }

    @Override
    public void flyForward(Double amount) {
        canNotFly();
    }

    @Override
    public void takeOff() {
        canNotFly();
    }

    @Override
    public void landing() {
        canNotFly();
    }

    @Override
    public void makeSound() {
        System.out.println("nod nod nod nod");
    }
}
