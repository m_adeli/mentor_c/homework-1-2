package ir.ac.kntu;

public class Main {
    public static void main(String[] args) {
        Cat maloos = new Cat(4);
        System.out.println(maloos);
        maloos.eatFood();
        maloos.makeSound();
        maloos.moveForward(10D);
        maloos.moveLeft(2D);
        System.out.println(maloos);
        maloos.sleep();

        Hen jojo = new Hen(1, 2D);
        System.out.println(jojo);
        jojo.takeOff();
        jojo.moveRight(3D);
        System.out.println(jojo);
        jojo.eatFood();
        jojo.makeSound();
        jojo.flyHigher(12D);
    }
}
