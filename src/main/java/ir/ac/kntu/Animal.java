package ir.ac.kntu;

public abstract class Animal implements Movable {
    private Integer age;
    private Double x;
    private Double y;
    private Double z;

    public Animal(Integer age) {
        this.age = age;
        this.x = 0D;
        this.y = 0D;
        this.z = 0D;
    }

    abstract void eatFood();

    void sleep() {
        System.out.println("Sleeping...zzz");
    }

    @Override
    public void moveUp(Double amount) {
        setZ(getZ() + amount);
    }

    @Override
    public void moveDown(Double amount) {
        setZ(getZ() - amount);
    }

    @Override
    public void moveRight(Double amount) {
        setY(getY() + amount);
    }

    @Override
    public void moveLeft(Double amount) {
        setY(getY() - amount);
    }

    @Override
    public void moveForward(Double amount) {
        setX(getX() + amount);
    }

    @Override
    public void moveBackward(Double amount) {
        setX(getX() - amount);
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public Double getZ() {
        return z;
    }

    public void setZ(Double z) {
        this.z = z;
    }
}
